stages:
  - build
  - test
  - docker
  - deploy

# Based on template from:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/dotNET-Core.yml

image: mcr.microsoft.com/dotnet/sdk:6.0

variables:
  # 1) Name of directory where restore and build objects are stored.
  OBJECTS_DIRECTORY: 'obj'
  # 2) Name of directory used for keeping restored dependencies.
  NUGET_PACKAGES_DIRECTORY: '.nuget'
  # 3) A relative path to the source code from project repository root.
  # NOTE: Please edit this path so it matches the structure of your project!
  SOURCE_CODE_PATH: './'

# ### Define global cache rule
#
# This example shows how to configure cache to pass over restored
# dependencies for re-use.
#
# With global cache rule, cached dependencies will be downloaded before every job
# and then unpacked to the paths as specified below.
cache:
  # Per-stage and per-branch caching.
  key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
  paths:
    # Specify three paths that should be cached:
    #
    # 1) Main JSON file holding information about package dependency tree, packages versions,
    # frameworks etc. It also holds information where to the dependencies were restored.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/project.assets.json'
    # 2) Other NuGet and MSBuild related files. Also needed.
    - '$SOURCE_CODE_PATH$OBJECTS_DIRECTORY/*.fsproj.nuget.*'
    # 3) Path to the directory where restored dependencies are kept.
    - '$NUGET_PACKAGES_DIRECTORY'
  policy: pull-push

# ### Restore project dependencies
#
# NuGet packages by default are restored to '.nuget/packages' directory
# in the user's home directory. That directory is out of scope of GitLab caching.
#
# To get around this, a custom path can be specified using the '--packages <PATH>' option
# for 'dotnet restore' command. In this example, a temporary directory is created
# in the root of project repository, so its content can be cached.
before_script:
  - 'dotnet restore --packages $NUGET_PACKAGES_DIRECTORY'

build:
  stage: build
  script:
    - 'dotnet build --no-restore'

test:
  stage: test
  script:
    - 'dotnet test --no-restore'

# Build Docker image
# See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-docker-executor-with-the-docker-image-docker-in-docker
build image:
  stage: docker
  image: docker:27.4.1
  variables:
    # See: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled
    DOCKER_TLS_CERTDIR: "/certs"
  services:
    - docker:27.4.1-dind

  before_script:
    # Login to container registry
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    # Tag with the unique pipeline id to generate a unique tag
    # See: https://docs.microsoft.com/en-us/azure/container-registry/container-registry-image-tag-version#unique-tags
    - docker build -t $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID .
    - docker push $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID

# Deploy new Docker image to Azure App Service
deploy:
  stage: deploy
  image: mcr.microsoft.com/azure-cli:2.67.0
  # Ensure only one deploy job runs at a time
  # https://docs.gitlab.com/ee/ci/environments/deployment_safety.html#ensure-only-one-deployment-job-runs-at-a-time
  resource_group: prod

  before_script:
    - az login --service-principal --username $AZ_SP_NAME --password $AZ_SP_PASSWORD --tenant $AZ_TENANT --output none
  script:
    - az webapp config container set --ids /subscriptions/$AZ_SUBSCRIPTION/resourceGroups/shorty/providers/Microsoft.Web/sites/shorty-be --docker-custom-image-name $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID --output none

  only:
    - main