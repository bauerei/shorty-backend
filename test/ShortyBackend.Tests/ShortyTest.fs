module ShortyBackend.Tests.Shorty

open NUnit.Framework
open FsUnit
open Shorty

[<Test>]
let ``shortFilmToJson with some short description should return json`` () =
    let shortFilm =
        { FilmUrl = "film.url"
          ShortDescription = "this film is about foo" 
          Title = "film title"
          Thumbnail = "//foo.bar/thumbnail.json" }

    let expected =
        $"""{{"film_url":"{shortFilm.FilmUrl}","short_description":"{shortFilm.ShortDescription}","title":"{shortFilm.Title}","thumbnail":"{shortFilm.Thumbnail}"}}"""

    shortFilmToJson shortFilm |> should equal expected