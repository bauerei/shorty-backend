module ShortyBackend.Tests.Sotw

open NUnit.Framework
open FsUnit
open Sotw

[<Test>]
let ``parseFilmPage should fail on empty json`` () =
    let json = "{}"

    match parseFilmPage json with
    | Ok _ -> Assert.Fail()
    | Error _ -> Assert.Pass()

[<Test>]
let ``parseFilmPage should parse single entry`` () =
    let anyMaxPage = 2782

    let anyUrl = "film.url"
    let anyShortDescription = "this film is about foo"
    let anyTitle = "film title"
    let anyThumbnail = "//foo.bar/thumbnail.json"

    let json =
        $"""
        {{
            "page_max": {anyMaxPage},
            "data": [
                {{
                    "play_link": "{anyUrl}",
                    "post_excerpt": "{anyShortDescription}",
                    "post_title": "{anyTitle}",
                    "thumbnail": "{anyThumbnail}"
                }}
            ]
        }}
        """

    let expectedFilmPage =
        { MaxPage = anyMaxPage
          ShortFilms =
            [ { FilmUrl = anyUrl
                ShortDescription = anyShortDescription 
                Title = anyTitle
                Thumbnail = anyThumbnail } ] }

    match parseFilmPage json with
    | Ok parsed -> parsed |> should equal expectedFilmPage
    | Error _ -> Assert.Fail()

[<Test>]
let ``parseFilmPage should parse multiple entries`` () =
    let anyMaxPage = 2782

    let anyUrl = "film1.url"
    let anyShortDescription = "this film is about foo"
    let anyTitle = "film title"
    let anyThumbnail = "//foo.bar/thumbnail.json"

    let anyOtherUrl = "film2.url"
    let anyOtherShortDescription = "this film is about bar"
    let anyOtherTitle = "other film title"
    let anyOtherThumbnail = "//foo.baz/thumbnail.json"

    let json =
        $"""
        {{
            "page_max": {anyMaxPage},
            "data": [
                {{
                    "play_link": "{anyUrl}",
                    "post_excerpt": "{anyShortDescription}",
                    "post_title": "{anyTitle}",
                    "thumbnail": "{anyThumbnail}"
                }},
                {{
                    "play_link": "{anyOtherUrl}",
                    "post_excerpt": "{anyOtherShortDescription}",
                    "post_title": "{anyOtherTitle}",
                    "thumbnail": "{anyOtherThumbnail}"
                }}
            ]
        }}
        """

    let expectedFilmPage =
        { MaxPage = anyMaxPage
          ShortFilms =
            [ { FilmUrl = anyUrl
                ShortDescription = anyShortDescription
                Title = anyTitle
                Thumbnail = anyThumbnail }
              { FilmUrl = anyOtherUrl
                ShortDescription = anyOtherShortDescription
                Title = anyOtherTitle
                Thumbnail = anyOtherThumbnail } ] }

    match parseFilmPage json with
    | Ok parsed -> parsed |> should equal expectedFilmPage
    | Error _ -> Assert.Fail()

[<Test>]
let ``getMaxPage should return max page`` () =
    let expectedMaxPage = 2782

    let json =
        $"""
        {{
            "page_max": {expectedMaxPage},
            "data": [
                {{
                    "play_link": "film.url",
                    "post_excerpt": "description",
                    "post_title": "title",
                    "thumbnail": "//foo.bar/thumbnail.json"
                }}
            ]
        }}
        """

    let fakeGet (_: string) = Async.result json

    match Async.RunSynchronously(getMaxPage fakeGet) with
    | Ok maxPage -> maxPage |> should equal expectedMaxPage
    | Error _ -> Assert.Fail()

[<Test>]
let ``getRandomPage should return url`` () =
    let anyPage = 2782

    let expectedUrl = "vimeo.com"

    let json =
        $"""
        {{
            "page_max": {anyPage},
            "data": [
                {{
                    "play_link": "{expectedUrl}",
                    "post_excerpt": "description",
                    "post_title": "title",
                    "thumbnail": "//foo.bar/thumbnail.json"
                }}
            ]
        }}
        """

    let fakeGet (_: string) = Async.result json

    match Async.RunSynchronously(getRandomFilm fakeGet anyPage) with
    | Ok filmPageEntry -> filmPageEntry.FilmUrl |> should equal expectedUrl
    | Error _ -> Assert.Fail()
