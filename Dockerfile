# Taken from: https://github.com/dotnet/dotnet-docker/blob/main/samples/dotnetapp/Dockerfile.alpine-x64

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /source

# copy fsproj and restore as distinct layers
COPY src/ShortyBackend/ShortyBackend.fsproj src/ShortyBackend/ShortyBackend.fsproj
RUN dotnet restore -r linux-musl-x64 src/ShortyBackend/

# copy and publish app and libraries
COPY src/ShortyBackend/ src/ShortyBackend/
RUN dotnet publish -c release -o /app -r linux-musl-x64 --no-self-contained --no-restore src/ShortyBackend/

# final stage/image
FROM mcr.microsoft.com/dotnet/runtime:6.0-alpine
WORKDIR /app
COPY --from=build /app .
ENTRYPOINT ["./ShortyBackend"]