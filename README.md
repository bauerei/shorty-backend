# Shorty-backend
Backend for bauerei/shorty>.

Provides an endpoint for getting a random short film from [shortoftheweek](https://www.shortoftheweek.com/).

## Usage

### Run server
To run the server you can either use [.NET CLI](https://docs.microsoft.com/en-us/dotnet/core/tools/) or [Docker](https://www.docker.com/).

#### Using `dotnet`
In the root directory of this project, run
```
dotnet run --project src/ShortyBackend/
```

#### Using `docker`

1. Build the Docker image. In the root directory of this project, run
```
docker build -t shorty-backend .
```

2. Run the server with
```
docker run -p 8080:8080 shorty-backend
```

### Send a request
Either way you started the server, you can now call
```
localhost:8080/api/random
```
to receive a random short film from [shortoftheweek](https://www.shortoftheweek.com/), e.g.
```
{"film_url":"https://player.vimeo.com/video/143734982"}
```
Yay! 🎉

## Credits
Credits go out to [shortoftheweek](https://www.shortoftheweek.com/) and all the filmmakers providing their content.

This project was build by me for fun. Except that it exploits the existing API from [shortoftheweek](https://www.shortoftheweek.com/), it is totally unrelated.

Furthermore, it relies on other great open source projects, which are listed along with their respective license [here](LICENSE-3RD-PARTY.md).



## License
This project is licensed under [MIT](LICENSE)
