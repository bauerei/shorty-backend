| Software | License |
| -------- | ------- |
| [Suave](http://suave.io/) *2.6.1* | [Apache license 2.0](https://github.com/SuaveIO/suave/blob/master/LICENSE) |
| [Thoth.Json.Net](https://thoth-org.github.io/) *7.1.0* | [MIT](https://github.com/thoth-org/Thoth.Json.Net/blob/master/LICENSE.md)