module Shorty

open Thoth.Json.Net

type ShortFilm =
    { FilmUrl: string
      ShortDescription: string
      Title: string
      Thumbnail: string }

let shortFilmToJson (shortFilm: ShortFilm) =
    Encode.Auto.toString (0, shortFilm, SnakeCase)

let getRandomFilm (get: string -> Async<string>) maxPage =
    async {
        match! Sotw.getRandomFilm get maxPage with
        | Ok filmPageEntry ->
                return
                    Ok
                        { FilmUrl = filmPageEntry.FilmUrl
                          ShortDescription = filmPageEntry.ShortDescription
                          Title = filmPageEntry.Title
                          Thumbnail = filmPageEntry.Thumbnail }
        | Error err -> return Error err
    }
