module Main

open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.ServerErrors
open System

let toHttpResponse (result: Async<Result<Shorty.ShortFilm, string>>) =
    fun (httpContext: HttpContext) ->
        async {
            match! result with
            | Ok shortFilm -> return! OK(Shorty.shortFilmToJson shortFilm) httpContext
            | Error err -> return! INTERNAL_ERROR err httpContext
        }

let get (httpClient: Net.Http.HttpClient) (url: string) =
    async {
        let! response = httpClient.GetAsync(url) |> Async.AwaitTask
        response.EnsureSuccessStatusCode() |> ignore

        return!
            response.Content.ReadAsStringAsync()
            |> Async.AwaitTask
    }

[<EntryPoint>]
let main argv =
    use httpClient = new System.Net.Http.HttpClient()

    let config =
        { defaultConfig with bindings = [ HttpBinding.createSimple HTTP "0.0.0.0" 8080 ] }

    let maxPage = Sotw.getMaxPage (get (httpClient))

    match Async.RunSynchronously(maxPage) with
    | Ok maxPage ->
        let app =
            choose [ path "/api/random"
                     >=> choose [ GET
                                  >=> toHttpResponse (Shorty.getRandomFilm (get httpClient) maxPage) ]
                     >=> Writers.addHeader "Access-Control-Allow-Origin" "*"
                     >=> Writers.setMimeType "application/json; charset=utf-8"
                     RequestErrors.NOT_FOUND "Found no handlers" ]

        startWebServer config app
        0
    | Error err ->
        System.Console.WriteLine($"Could not initialize maxPage: {err}")
        -1
