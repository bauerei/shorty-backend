module Sotw

open System
open Thoth.Json.Net

type FilmPageEntry =
    { FilmUrl: String
      ShortDescription: String 
      Title: String 
      Thumbnail: String }

type FilmPage =
    { MaxPage: int
      ShortFilms: FilmPageEntry list }

let parseFilmPage string =
    let filmPageEntryDecoder: Decoder<FilmPageEntry> =
        Decode.map4
            (fun filmUrl shortDescription title thumbnail ->
                { FilmUrl = filmUrl
                  ShortDescription = shortDescription 
                  Title = title 
                  Thumbnail = thumbnail }: FilmPageEntry)
            (Decode.field "play_link" Decode.string)
            (Decode.field "post_excerpt" Decode.string)
            (Decode.field "post_title" Decode.string)
            (Decode.field "thumbnail" Decode.string)

    let filmPageDecoder: Decoder<FilmPage> =
        Decode.object (fun get ->
            { MaxPage = (get.Required.Field "page_max" Decode.int)
              ShortFilms = (get.Required.Field "data" (Decode.list filmPageEntryDecoder)) })

    Decode.fromString filmPageDecoder string

let private getFilmPage (get: string -> Async<string>) (page: int) =
    async {
        let! filmPage = get $"https://www.shortoftheweek.com/api/v1/films?page={page}"
        return parseFilmPage filmPage
    }

let getMaxPage (get: string -> Async<string>) =
    async {
        let! response = getFilmPage get 1

        return response |> Result.map (fun res -> res.MaxPage)
    }

let getRandomFilm (get: string -> Async<string>) maxPage =
    let random = Random()

    // Picking a random list element
    // Taken from: https://stackoverflow.com/a/33316100
    let shuffle list =
        list
        |> Seq.sortBy (fun _ -> random.Next())
        |> Seq.head

    async {
        match! getFilmPage get (random.Next(1, maxPage + 1)) with // SOTW pages start with 1 and end with maxPage
        | Ok result ->
                return result.ShortFilms
                |> List.filter (fun filmPageEntry -> filmPageEntry.FilmUrl.Contains("vimeo"))
                |> shuffle
                |> Ok
        | Error err -> return Error err
    }
